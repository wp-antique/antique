<?php
/**
 * The template for displaying sidebars.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/
 *
 * @package Antique
 * @since Antique 1.0
 */

if (is_page()) {
    get_template_part('template-parts/sidebar/sidebar-page');
} elseif (is_single()) {
    get_template_part('template-parts/sidebar/sidebar-single');
}