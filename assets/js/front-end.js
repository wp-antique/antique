jQuery(document).ready(function ($) {

    function is_mobile() {
        if ($(window).width() <= 768) {
            return true;
        } else {
            return false;
        }
    }

    // --- Navigation bar ---
    // The script adds arrows to menu items that have a submenu
    // and makes the navigation bar responsive (its appearance is different
    // on mobile devices).
    var arrow_icon = document.createElement('i');
    arrow_icon.classList.add('fa');
    arrow_icon.classList.add('fa-angle-down');

    var arrow = document.createElement('span');
    arrow.classList.add('sub-menu-arrow');
    arrow.appendChild(arrow_icon);

    $('#site-navigation .sub-menu').each(function () {
        $(this).parent().addClass('has-sub-menu');
        $(this).parent().append(arrow.cloneNode(true));
    });

    $('.sub-menu-arrow').click(function () {
        $(this).toggleClass('rotate-arrow');
        $(this).siblings('.sub-menu').toggleClass('show');
    });

    $('.mobile-nav-toggle').click(function () {
        $('.mobile-nav-opener-icon').toggleClass('make-cross');
        $('.mobile-dark-layer').toggleClass('make-dark');
        $('.nav-bar').toggleClass('move-in');
    });

    // --- Post navigation bars ---
    // Adds space between both post navigation bars.

    if ($('#post-ancestors-bar').length && $('#next-previous-bar').length) {
        var post_nav_sep = document.createElement('div');
        post_nav_sep.classList.add('post-nav-separator');
        $('#post-ancestors-bar').after(post_nav_sep);
    }

    // --- Sidebar ---
    // The top sidebar is moved before the content on mobile devices.
    function move_top_sidebar() {
        if (is_mobile()) {
            $('.post-content-wrap').first().before($('#top-sidebar'));
        } else {
            $('.post-content-wrap').first().after($('#top-sidebar'));
        }
    }

    if ($('#top-sidebar').length) {
        move_top_sidebar();

        $(window).on('resize', function () {
            move_top_sidebar();
        });
    }

    // --- Reading progress bar ---
    // Updates the progress bar when scrolling.
    function get_window_height() {
        return window.innerHeight;
    }

    function get_max() {
        return $(document).height() - get_window_height();
    }

    function get_pos() {
        var val = $(window).scrollTop();
        if (val > 0) {
            return val;
        } else {
            return 0;
        }
    }

    var $progress_bar = $('#reading-progress-bar');

    $progress_bar.attr({max: get_max()});
    $progress_bar.attr({value: get_pos()});

    $(document).on('scroll', function () {
        $progress_bar.attr({value: get_pos()});
    });

    $(window).on('resize touchmove', function () {
        $progress_bar.attr({
            max: get_max(),
            value: get_pos()
        });
    });

    // --- Jump to top ---
    // After having scrolled a certain percentage of the document  
    // height (which is specified by the parameter 'threshold') a button-like
    // element appears on mobile devices. By touching it one returns to the
    // top of the document.
    function show_to_top(threshold) {
        if (get_pos() > (threshold / 100) * get_max()) {
            $('#mobile-to-top').removeClass('disappear');
        } else {
            $('#mobile-to-top').addClass('disappear');
        }
    }

    show_to_top(threshold = 5);

    $(document).on('scroll', function () {
        show_to_top(threshold = 5);
    });

    $('#mobile-to-top').on('touchstart click', function () {
        $('html, body').animate({scrollTop: 0}, 'fast');
    });

    // --- Turn off resizing on text inputs ---
    // Touching text inputs on small screens (e.g. of smartphones)
    // triggers resizing due to the appearing keyboard. Therefore resizing
    // is turned off.
    $('input').on('touchstart', function () {
        $(window).off('resize');
    });

    $('textarea').on('touchstart', function () {
        $(window).off('resize');
    });

    // --- Admin bar ---
    // If the admin bar is displayed, it overlaps with the mobile 
    // navigation opener and the reading progress bar. They are moved down in 
    // order to make these fixed elements visible again.
    function move_fixed_elements() {
        var $admin_bar_height = $('#wpadminbar').height();

        if (is_mobile()) {
            $bar_top = $admin_bar_height + parseFloat($('.mobile-nav-opener').css('height'));
            $('#reading-progress-bar').css('top', $bar_top + 'px');
            $('.mobile-nav-opener').css('top', $admin_bar_height + 'px');
        } else {
            $('#reading-progress-bar').css('top', $admin_bar_height + 'px');
        }
    }

    if ($('#wpadminbar').length) {
        move_fixed_elements();

        $(window).on('resize', function () {
            move_fixed_elements();
        });
    }
});