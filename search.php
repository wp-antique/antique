<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php get_header(); ?>

<header id="page-header" class="site-page-header">
    <div class="wrapper">
        <div class="page-header-inner-wrap">
            <div class="page-title-wrap">

                <h1 class="page-title"><?php
                    printf(
                            esc_html__('Results for “%s”', 'antique'),
                            get_search_query()
                    );
                    ?></h1>
                <span class="page-subtitle">
                    <?php
                    $found_results = (int) $wp_query->found_posts;

                    if ($found_results == 0) {
                        esc_html_e(
                                'There are no results for your search.',
                                'antique'
                        );
                    } else if ($found_results == 1) {
                        esc_html_e(
                                'There is 1 result for your search.',
                                'antique'
                        );
                    } else {
                        printf(
                                esc_html__(
                                        'There are %d results for your search.',
                                        'antique'
                                ),
                                (int) $wp_query->found_posts
                        );
                    }
                    ?>
                </span>

            </div>
        </div>
    </div>
</header>


<div id="page-content-area" class="site-page-content-area">
    <div class="wrapper adjust-overflow">
        <div class="site-page-content">

            <?php
            if (have_posts()) {

                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/excerpt/excerpt');
                }

                antique_theme_posts_pagination();
            } else {
                get_template_part('template-parts/content/content-none');
            }
            ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>
