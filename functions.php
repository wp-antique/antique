<?php
/**
 * Functions and addition of actions and filters.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Antique
 * @since Antique 1.0
 */
/*
 * Table of contents
 * -----------------
 * 1 Admin area
 *      1.1 Settings
 *      1.2 Styles and scripts
 * 2 Theme settings
 *      2.1 Setup
 *      2.2 Styles and scripts
 *      2.3 Admin bar
 *      2.4 Sidebars and widgets
 * 3 Meta functions
 *      3.1 Thumbnail
 *      3.2 Post navigation bars
 *          3.2.1 Ancestors bar
 *          3.2.2 Next-previous bar
 *      3.3 Antique sidebar blocks
 * 4 Template tags
 *      4.1 Post information
 *      4.2 Pagination
 */

require get_template_directory() . '/customizer.php';

/*
  ----------------------------------------
  1 ADMIN AREA
  ----------------------------------------
 */

/*
  ----------------------------------------
  1.1 Settings
  ----------------------------------------
 */

add_action(
        hook_name: 'admin_menu',
        callback: 'antique_theme_remove_posts_and_comments'
);

if (!function_exists('antique_theme_remove_posts_and_comments')) {

    /**
     * Disables blog posts and comments. Support will be added to the theme
     * at a later point in time.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_remove_posts_and_comments() {
        remove_menu_page(menu_slug: 'edit.php');
        remove_menu_page(menu_slug: 'edit-comments.php');
    }

}

/*
  ----------------------------------------
  1.2 Styles and scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'admin_enqueue_scripts',
        callback: 'antique_theme_admin_scripts'
);

if (!function_exists('antique_theme_admin_scripts')) {

    /**
     * Registers and enqueues styles and scripts for all admin pages.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_admin_scripts() {

        wp_register_script(
                handle: 'antique-theme-admin',
                src: get_template_directory_uri() . '/assets/js/admin.js',
                deps: array('wp-color-picker'),
                ver: false,
                in_footer: true
        );

        wp_enqueue_script(handle: 'antique-theme-admin');
        wp_enqueue_style(handle: 'wp-color-picker');
    }

}

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_theme_block_editor_scripts'
);

if (!function_exists('antique_theme_block_editor_scripts')) {

    /**
     * Registers and enqueues styles and scripts for the editing interface.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_block_editor_scripts() {

        wp_register_style(
                handle: 'antique-theme-meta-boxes',
                src: get_template_directory_uri() . '/assets/css/meta-boxes.css',
                deps: array(),
                ver: false,
                media: 'all'
        );

        wp_enqueue_style(handle: 'antique-theme-meta-boxes');
    }

}

/*
  ----------------------------------------
  2 THEME SETTINGS
  ----------------------------------------
 */

/*
  ----------------------------------------
  2.1 Setup
  ----------------------------------------
 */

add_action(
        hook_name: 'after_setup_theme',
        callback: 'antique_theme_setup'
);

if (!function_exists('antique_theme_setup')) {

    /**
     * Sets up theme defaults and adds support for WordPress features.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_setup() {

        load_theme_textdomain(
                domain: 'antique',
                path: get_template_directory() . '/languages'
        );

        register_nav_menus(
                array(
                    'primary' => esc_html__('Primary menu', 'antique'),
                )
        );

        add_theme_support(
                'custom-logo',
                array(
                    'size' => 'medium',
                )
        );

        add_theme_support(feature: 'post-thumbnails');

        add_theme_support(feature: 'title-tag');

        add_theme_support(feature: 'editor-styles');
    }

}

/*
  ----------------------------------------
  2.2 Styles and scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'wp_enqueue_scripts',
        callback: 'antique_theme_front_end_scripts'
);

if (!function_exists('antique_theme_front_end_scripts')) {

    /**
     * Registers and enqueues styles and scripts for all posts (front end).
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_front_end_scripts() {

        wp_register_script(
                handle: 'antique-theme-front-end',
                src: get_template_directory_uri() . '/assets/js/front-end.js',
                deps: array('jquery'),
                ver: false,
                in_footer: true
        );

        wp_register_style(
                handle: 'antique-font-awesome',
                src: 'https://use.fontawesome.com/releases/v6.4.0/css/all.css',
                deps: array(),
                ver: false,
                media: 'all'
        );

        wp_enqueue_script(handle: 'jquery');
        wp_enqueue_script(handle: 'antique-theme-front-end');
        wp_enqueue_style(handle: 'antique-font-awesome');
    }

}

/*
  ----------------------------------------
  2.3 Admin bar
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_theme_display_admin_bar'
);

if (!function_exists('antique_theme_display_admin_bar')) {

    /**
     * Displays the admin bar on the front end if the user is logged in.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_display_admin_bar() {

        if (is_user_logged_in()) {
            add_filter(
                    hook_name: 'show_admin_bar',
                    callback: '__return_true',
                    priority: 1000
            );
        }
    }

}

/*
  ----------------------------------------
  2.4 Sidebars and widgets
  ----------------------------------------
 */

add_action(
        hook_name: 'widgets_init',
        callback: 'antique_theme_register_sidebars'
);

if (!function_exists('antique_theme_register_sidebars')) {

    /**
     * Registers all sidebars.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_register_sidebars() {

        register_sidebar(
                array(
                    'name' => __('Top sidebar (pages)', 'antique'),
                    'id' => 'pages_top_sidebar',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Bottom sidebar (pages)', 'antique'),
                    'id' => 'pages_bottom_sidebar',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Top sidebar (posts)', 'antique'),
                    'id' => 'posts_top_sidebar',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Bottom sidebar (posts)', 'antique'),
                    'id' => 'posts_bottom_sidebar',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Footer widget area', 'antique') . ' (1)',
                    'id' => 'footer_widget_area_1',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Footer widget area', 'antique') . ' (2)',
                    'id' => 'footer_widget_area_2',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Footer widget area', 'antique') . ' (3)',
                    'id' => 'footer_widget_area_3',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );

        register_sidebar(
                array(
                    'name' => __('Footer widget area', 'antique') . ' (4)',
                    'id' => 'footer_widget_area_4',
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget' => '</section>',
                    'before_title' => '<span class="widget-title">',
                    'after_title' => '</span>',
                )
        );
    }

}

if (!function_exists('antique_theme_is_active_top_sidebar')) {

    /**
     * Checks if the top sidebar has any content. Returns true if the post
     * has a thumbnail, if any of the Antique blocks is selected to be
     * displayed in the sidebar or if the user has inserted custom widgets
     * in the theme customizer.
     *
     * @since Antique 1.0
     *
     * @param string $post_type The post type of the currently displayed post.
     * @return bool
     */
    function antique_theme_is_active_top_sidebar($post_type) {

        $tn_position = antique_theme_get_thumbnail_settings()['position'];

        $active = array(
            has_post_thumbnail() && $tn_position == 'in-sidebar',
            antique_theme_is_active_sidebar_blocks(),
        );

        if ($post_type == 'page') {
            $active[] = is_active_sidebar('pages_top_sidebar');
        } elseif ($post_type == 'post') {
            $active[] = is_active_sidebar('posts_top_sidebar');
        } else {
            return false;
        }

        return in_array(true, $active, true) ?: false;
    }

}

if (!function_exists('antique_theme_is_active_sidebar_blocks')) {

    /**
     * Checks if any of the Antique plugins containing a block is activated.
     *
     * @since Antique 1.0
     *
     * @return bool
     */
    function antique_theme_is_active_sidebar_blocks() {

        if (!function_exists('antique_plugins_get_info')) {
            return false;
        }

        $active = array();
        $plugins = antique_plugins_get_info();

        foreach ($plugins as $info) {
            if ($info['has-sidebar-meta']) {
                $active[] = $info['is-active'];
            }
        }

        return in_array(true, $active, true) ?: false;
    }

}

if (!function_exists('antique_theme_is_active_footer_widget_areas')) {

    /**
     * Checks if any of the widget areas in the footer is active.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_is_active_footer_widget_areas() {

        return (is_active_sidebar('footer_widget_area_1') ||
                is_active_sidebar('footer_widget_area_2') ||
                is_active_sidebar('footer_widget_area_3') ||
                is_active_sidebar('footer_widget_area_4'));
    }

}

/*
  ----------------------------------------
  2.5 Excerpts
  ----------------------------------------
 */

add_filter(
        hook_name: 'excerpt_more',
        callback: 'antique_theme_excerpt_more'
);

if (!function_exists('antique_theme_excerpt_more')) {

    /**
     * Modifies the string in the 'more' link displayed after a trimmed
     * excerpt.
     *
     * @since Antique 1.0
     *
     * @param string $more The string shown within the more link.
     * @return string
     */
    function antique_theme_excerpt_more($more) {

        if (!is_single()) {
            $more = sprintf(
                    ' ... <a class="read-more" href="%1$s">%2$s</a>',
                    get_permalink(get_the_ID()),
                    __('read more', 'antique')
            );
        }

        return $more;
    }

}

if (!function_exists('antique_theme_excerpt_page_ancestors')) {

    /**
     * Displays the page ancestors in an excerpt.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_excerpt_page_ancestors() {

        $ancestors = array_reverse(
                get_post_ancestors(post: get_the_ID())
        );

        $list = '<div class="excerpt-post-ancestors">';

        for ($i = 0; $i < count($ancestors); $i++) {

            $ancestor = $ancestors[$i];
            $title = esc_html(get_the_title(post: $ancestor));
            $url = esc_url(get_permalink(post: $ancestor));

            $list .= '<a href="' . $url . '">' . $title . '</a> &raquo; ';
        }

        $title = esc_html(get_the_title(post: get_the_ID()));
        $url = esc_url(get_permalink(post: get_the_ID()));

        $list .= '<a href="' . $url . '">' . $title . '</a>';
        $list .= '</div>';

        echo $list;
    }

}

/*
  ----------------------------------------
  3 META FUNCTIONS
  ----------------------------------------
 */

if (!function_exists('antique_theme_parse_meta_args')) {

    /**
     * Recursively merges an array with user-defined values into an array
     * of default values.
     *
     * A big thank you to Andrei Surdu (see user-contributed notes):
     * https://developer.wordpress.org/reference/functions/wp_parse_args/
     *
     * @since Antique 1.0
     *
     * @param array $args     Array with user-defined values that override the
     * default values.
     * @param array $defaults Array with default values.
     * @return array
     */
    function antique_theme_parse_meta_args($args, $defaults) {

        $new_args = (array) $defaults;

        if (empty($args)) {
            return $new_args;
        }

        foreach ($args as $key => $value) {
            if (is_array($value) && isset($new_args[$key])) {
                $new_args[$key] = antique_theme_parse_meta_args(
                        args: $value,
                        defaults: $new_args[$key]
                );
            } else {
                $new_args[$key] = $value;
            }
        }

        return $new_args;
    }

}

if (!function_exists('antique_theme_truncate_string')) {

    /**
     * Truncates a string and appends three dots.
     *
     * @since Antique 1.0
     *
     * @param string $string  The string to be truncated.
     * @param int    $length  Maximum string length.
     * @return string
     */
    function antique_theme_truncate_string($string, $length) {

        if (strlen($string) >= $length) {
            return substr($string, 0, $length) . " ... ";
        } else {
            return $string;
        }
    }

}

if (!function_exists('antique_theme_get_all_meta_keys')) {

    /**
     * Returns all existing meta keys for the post types 'post' and 'page'.
     *
     * @since Antique 1.0
     *
     * @param string $echo  If true, the meta keys are output with echo. 
     * If false, the meta keys are returned in an array.
     * @return array|void
     */
    function antique_theme_get_all_meta_keys($echo = true) {

        $meta_keys = array();
        $posts = get_posts(array(
            'post_type' => array(
                'post',
                'page'
            ),
            'numberposts' => -1,
            'posts_per_page' => -1
        ));

        if (empty($posts)) {
            $posts = array();
        }

        foreach ($posts as $post) {
            $post_meta_keys = get_post_custom($post->ID);
            $meta_keys = array_merge($meta_keys, array_keys($post_meta_keys));
        }

        $meta_keys = array_values(array_unique($meta_keys));

        if ($echo) {
            foreach ($meta_keys as $meta_key) {
                echo $meta_key . '<br>';
            }
        } else {
            return $meta_keys;
        }
    }

}

/*
  ----------------------------------------
  3.1 Thumbnail
  ----------------------------------------
 */

require get_template_directory() . '/meta/thumbnail.php';

if (!function_exists('antique_theme_get_thumbnail_settings')) {

    /**
     * Retrieves the post meta and returns the thumbnail settings.
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_get_thumbnail_settings() {

        if (!is_page() && !is_single()) {
            return array();
        }

        $post_meta = antique_theme_thumbnail_get_post_meta(
                id: get_the_ID()
        );

        $settings = array(
            'position' => $post_meta['position'],
            'caption' => $post_meta['caption'],
        );

        if (
                $post_meta['position'] == 'before-content' ||
                $post_meta['position'] == 'in-sidebar'
        ) {
            $settings['aspect-ratio'] = sprintf(
                    '%s / %s',
                    $post_meta['aspect-ratio'][$post_meta['position']]['x'],
                    $post_meta['aspect-ratio'][$post_meta['position']]['y']
            );
        }

        return $settings;
    }

}

/*
  ----------------------------------------
  3.2 Post navigation bars
  ----------------------------------------
 */

require get_template_directory() . '/meta/post-navs.php';

/*
  ----------------------------------------
  3.2.1 Ancestors bar
  ----------------------------------------
 */

if (!function_exists('antique_theme_get_post_ancestors')) {

    /**
     * Retrieves the post meta and returns a user-defined number of page
     * ancestors (if the post ancestors bar is activated).
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_get_post_ancestors() {

        $post_meta = antique_theme_post_navs_get_post_meta(
                        id: get_the_ID()
                )['ancestors-bar'];

        if (empty($post_meta['display'])) {
            return array();
        }

        $ancestors = array_reverse(
                array_slice(
                        array: get_post_ancestors(post: get_the_ID()),
                        offset: 0,
                        length: $post_meta['number']
                )
        );

        return $ancestors;
    }

}

/*
  ----------------------------------------
  3.2.2 Next-previous bar
  ----------------------------------------
 */

if (!function_exists('antique_theme_get_next_previous_pages')) {

    /**
     * Retrieves the post meta and returns the pages that have been selected
     * to be displayed as previous/next page (if the next-previous bar is
     * activated).
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_get_next_previous_pages() {

        $pages = array(
            'previous' => NULL,
            'next' => NULL
        );

        $post_type = get_post_type();

        if ('page' == $post_type) {

            $post_meta = antique_theme_post_navs_get_post_meta(
                            id: get_the_ID()
                    )['next-previous-bar'];

            if (empty($post_meta['display'])) {
                return $pages;
            }

            if (!empty($post_meta['previous'])) {
                $pages['previous'] = get_post(post: $post_meta['previous']);
            }

            if (!empty($post_meta['next'])) {
                $pages['next'] = get_post(post: $post_meta['next']);
            }
        }

        if ('post' == $post_type) {

            $pages['previous'] = get_next_post();
            $pages['next'] = get_previous_post();
        }

        return $pages;
    }

}

/*
  ----------------------------------------
  3.3 Antique sidebar blocks
  ----------------------------------------
 */

require get_template_directory() . '/meta/sidebar-blocks.php';

if (!function_exists('antique_theme_display_sidebar_blocks')) {

    /**
     * Retrieves the post meta and displays the selected Antique blocks
     * in the (top) sidebar.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_display_sidebar_blocks() {

        if (!function_exists('antique_plugins_get_info')) {
            echo '';
        }

        $post_meta = antique_theme_sidebar_blocks_get_post_meta(
                id: get_the_ID()
        );

        $plugins = antique_plugins_get_info();

        foreach ($plugins as $name => $info) {

            if (!($info['is-active'] && $info['has-sidebar-meta'])) {
                continue;
            }

            $sidebar_values = $post_meta[$info['domain']];

            if ($sidebar_values['display'] != 'on') {
                continue;
            }

            if ($name == 'antique-reading-time') {
                $add_subpages = $sidebar_values['add_subpages'] == 'on';
                $attributes = $add_subpages ? '{"addSubpages":true} ' : '';
            } else {
                $attributes = '';
            }

            echo '<section class="widget">';
            echo do_blocks('<!-- wp:' . $name . '/block ' . $attributes . '/-->');
            echo '</section>';
        }
    }

}

/*
  ----------------------------------------
  4 TEMPLATE TAGS
  ----------------------------------------
 */

/*
  ----------------------------------------
  4.1 Post information
  ----------------------------------------
 */

if (!function_exists('antique_theme_post_author')) {

    /**
     * Prints HTML containing the post author.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_post_author() {

        if (post_type_supports(get_post_type(), 'author')) {

            $author = '<span class="post-author post-meta">';

            $author .= sprintf(
                    esc_html__('Author: %s.', 'antique'),
                    sprintf(
                            '<a href="%s" rel="author">%s</a>',
                            esc_url(get_author_posts_url(get_the_author_meta('ID'))),
                            esc_html(get_the_author())
                    )
            );

            $author .= '</span>';

            echo $author;
        }
    }

}

if (!function_exists('antique_theme_post_date')) {

    /**
     * Prints HTML containing the publication date of a post.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_post_date() {

        $time_tag = sprintf(
                '<time datetime="%s">%s</time>',
                esc_attr(get_the_date(format: DATE_W3C)),
                esc_html(get_the_date())
        );

        $date = '<span class="post-date post-meta">';
        $date .= sprintf(
                __('Published: %s.', 'antique'),
                sprintf(
                        '<a href="%s">%s</a>',
                        esc_url(
                                get_month_link(
                                        year: intval(get_the_time('Y')),
                                        month: intval(get_the_time('m'))
                                )
                        ),
                        $time_tag
                )
        );
        $date .= '</span>';

        echo $date;
    }

}

if (!function_exists('antique_theme_post_last_update')) {

    /**
     * Prints HTML containing the date of the last post update.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_post_last_update() {

        $time_tag = sprintf(
                '<time datetime="%s">%s</time>',
                esc_attr(get_the_modified_date(format: DATE_W3C)),
                esc_html(get_the_modified_date())
        );

        $date = '<span class="post-date post-meta">';
        $date .= sprintf(
                esc_html__('Published: %s.', 'antique'),
                $time_tag
        );
        $date .= '</span>';

        echo $date;
    }

}

if (!function_exists('antique_theme_single_taxonomies')) {

    /**
     * Prints HTML with information about the (single) post taxonomy (i.e.
     * categories and tags).
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_single_taxonomies() {

        if (has_category() || has_tag()) {

            $taxonomies = '';

            $categories = get_the_category_list(
                    separator: wp_get_list_item_separator()
            );
            if ($categories) {
                $taxonomies .= '<span class="cat-links post-meta">';
                $taxonomies .= sprintf(
                        esc_html__('Categories: %s', 'antique'),
                        $categories
                );
                $taxonomies .= '</span>';
            }

            $tags = get_the_tag_list(
                    sep: wp_get_list_item_separator()
            );
            if ($tags) {
                $taxonomies .= '<span class="tags-links post-meta">';
                $taxonomies .= sprintf(
                        esc_html__('Tags: %s', 'antique'),
                        $tags
                );
                $taxonomies .= '</span>';
            }

            echo $taxonomies;
        }
    }

}

if (!function_exists('antique_theme_about_author')) {

    /**
     * Prints HTML with image and description of the post author.
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_about_author() {

        $description = get_the_author_meta('description');

        if ($description) {

            $about = '<div class="about-author post-meta">';

            if (!is_author()) {
                $about .= sprintf(
                        '<span class="author-name">%s</span>',
                        esc_html(get_the_author())
                );
            }

            $about .= get_avatar(
                            id_or_email: get_the_author_meta('ID'),
                            size: 96,
                            args: array(
                                'class' => 'author-avatar',
                            )
                    )
                    . '<p class="author-description">' . $description . '</p>'
                    . '</div>';

            echo $about;
        }
    }

}

if (!function_exists('antique_theme_current_author')) {

    /**
     * Returns the author on an author archive page.
     *
     * @link https://codex.wordpress.org/Author_Templates
     *
     * @since Antique 1.0
     *
     * @return string
     */
    function antique_theme_current_author() {

        if (!is_archive()) {
            return '';
        }

        if (get_query_var('author_name')) {
            $user = get_user_by('slug', get_query_var('author_name'));
        } else {
            $user = get_userdata(get_query_var('author'));
        }

        $curauth = $user ? $user->display_name : NULL;

        return $curauth;
    }

}

/*
  ----------------------------------------
  4.2 Pagination
  ----------------------------------------
 */

if (!function_exists('antique_theme_posts_pagination')) {

    /**
     * Displays a paginated navigation to the next/previous posts.
     *
     * @link https://developer.wordpress.org/themes/functionality/pagination/
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_posts_pagination() {

        if (!is_paged() && !is_search()) {
            return;
        }

        the_posts_pagination(
                array(
                    'prev_next' => true,
                    'prev_text' => '&laquo;',
                    'next_text' => '&raquo;',
                    'end_size' => 1,
                    'mid_size' => 1,
                    'class' => 'pagination',
                )
        );
    }

}

if (!function_exists('antique_theme_post_pagination')) {

    /**
     * Prints the page navigation for a post that is divided into multiple
     * pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/pagination/
     *
     * @since Antique 1.0
     *
     * @return void
     */
    function antique_theme_post_pagination() {

        global $numpages;

        if (!($numpages > 1)) {
            return;
        }

        $pagination = sprintf(
                '<nav class="page-navigation pagination" aria-label="%s">',
                esc_html__('pages'),
        );

        $pagination .= sprintf(
                '<h2 class="screen-reader-text">%s</h2>',
                esc_html__('Page navigation'),
        );

        $pagination .= '<div class="post-nav-links">';

        $pagination .= wp_link_pages(
                args: array(
                    'before' => '',
                    'after' => '',
                    'echo' => false,
                )
        );

        $pagination .= '</div>';
        $pagination .= '</nav>';

        echo $pagination;
    }

}