��    C      4  Y   L      �     �     �     �     �     �  !   �          /     J     h          �     �     �     �     �     �     �     �       	     
   "     -  �   4     �     �     �       7     
   N     Y  
   i     t     |     �     �  	   �     �     �     �     �     �     �     
	     	     4	     C	     K	     S	  %   \	  %   �	  "   �	  p   �	  '   <
     d
     y
     
     �
     �
     �
     �
  
   �
     �
     �
  	   �
     �
  �       �     �     �  
          !   "     D     _     {     �     �     �     �     �          
          .     F     _  
   p     {     �  �   �          $     @     W  >   o     �     �     �     �     �            	   ,     6     C     P     a  
   z     �     �     �  
   �     �     �     �  &   �  )     #   D  y   h  2   �          2     8     T      o  
   �  
   �     �     �     �     �     �     5                 B   A   (             <   "       7       $         1   -   3             &   :         *   ?          4           @             C           6   ,   /   2                                     
         +   #   .   9          >             %       '      8      ;   =         )   0      !             	       Antique blocks Archive Aspect ratio: Author: %s. Background color Background gradient: bottom right Background gradient: left Background gradient: right Background gradient: top left Bottom sidebar (pages) Bottom sidebar (posts) Caption: Categories: %s Category: %s Colors Content Content background color Content border color Content font color Display bar: Error 404 Font color Footer For thumbnails displayed in the sidebar it is recommended to use an aspect ratio of 3/4, 2/3 or 1/1, for thumbnails displayed below the title 4/1 or 5/1. Header Heading background color Heading border color Heading font color If you don't select any page, the bar is not displayed. Link color Navigation bars Next page: Number: Page navigation Page not found Parent pages Position: Posts by %s Posts from %s Previous page: Previous/next page Primary menu Published: %s. Reading progress bar Results for “%s” Secondary menu Sidebar Tag: %s Tags: %s There are %d results for your search. There are no results for your search. There is 1 result for your search. This is a top-level page and doesn't have any parent pages. Therefore, the bar cannot be displayed on this page. This page does not exist. Return to %s? Thumbnail (settings) Title Top sidebar (pages) Top sidebar (posts) Visited link color before content homepage in sidebar next to title pages read more select Project-Id-Version: Antique Theme
PO-Revision-Date: 2023-06-30 09:36+0200
Last-Translator: 
Language-Team: Simon Garbin
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e;_n
X-Poedit-SearchPath-0: 404.php
X-Poedit-SearchPath-1: archive.php
X-Poedit-SearchPath-2: customizer.js
X-Poedit-SearchPath-3: customizer.php
X-Poedit-SearchPath-4: footer.php
X-Poedit-SearchPath-5: functions.php
X-Poedit-SearchPath-6: header.php
X-Poedit-SearchPath-7: home.php
X-Poedit-SearchPath-8: index.php
X-Poedit-SearchPath-9: page.php
X-Poedit-SearchPath-10: search.php
X-Poedit-SearchPath-11: searchform.php
X-Poedit-SearchPath-12: sidebar.php
X-Poedit-SearchPath-13: single.php
X-Poedit-SearchPath-14: template-parts/content/content-none.php
X-Poedit-SearchPath-15: template-parts/content/content-page.php
X-Poedit-SearchPath-16: template-parts/content/content-single.php
X-Poedit-SearchPath-17: template-parts/excerpt/excerpt.php
X-Poedit-SearchPath-18: template-parts/excerpt/excerpt-page.php
X-Poedit-SearchPath-19: template-parts/excerpt/excerpt-single.php
X-Poedit-SearchPath-20: template-parts/extras/mobile-nav.php
X-Poedit-SearchPath-21: template-parts/extras/next-previous-bar.php
X-Poedit-SearchPath-22: template-parts/extras/post-ancestors-bar.php
X-Poedit-SearchPath-23: template-parts/extras/progress-bar.php
X-Poedit-SearchPath-24: template-parts/header/post-header.php
X-Poedit-SearchPath-25: template-parts/header/site-branding.php
X-Poedit-SearchPath-26: template-parts/header/site-header.php
X-Poedit-SearchPath-27: template-parts/header/site-nav.php
X-Poedit-SearchPath-28: template-parts/sidebar/sidebar-page.php
X-Poedit-SearchPath-29: template-parts/sidebar/sidebar-single.php
X-Poedit-SearchPath-30: template-parts/thumbnail/thumbnail-before-content.php
X-Poedit-SearchPath-31: template-parts/thumbnail/thumbnail-in-sidebar.php
X-Poedit-SearchPath-32: template-parts/thumbnail/thumbnail-next-to-title.php
X-Poedit-SearchPath-33: meta/sidebar-blocks.php
X-Poedit-SearchPath-34: meta/thumbnail.php
X-Poedit-SearchPath-35: meta/post-navs.php
 Antique-Blöcke Archiv Seitenverhältnis: Autor: %s. Hintergrundfarbe Hintergrundgradient: rechts unten Hintergrundgradient: links Hintergrundgradient: rechts Hintergrundgradient: links oben Untere Seitenleiste (Seiten) Untere Seitenleiste (Posts) Bildunterschrift: Kategorien: %s Kategorie: %s Farben Inhalt Hintergrundfarbe des Inhalts Rahmenfarbe des Inhalts Schriftfarbe des Inhalts Leiste anzeigen: Fehler 404 Schriftfarbe Footer Für Thumbnails in der Seitenleiste wird ein Seitenverhältnis von 3/4, 2/3 oder 1/1 empfohlen, für Thumbnails unter dem Titel 4/1 oder 5/1. Header Hintergrundfarbe des Titels Rahmenfarbe des Titels Schriftfarbe des Titels Wenn du keine Seiten wählst, wird die Leiste nicht angezeigt. Schriftfarbe von Links Navigationsleisten Nächste Seite: Anzahl: Seitennavigation Seite nicht gefunden Übergeordnete Seiten Position: Posts von %s Posts vom %s Vorherige Seite: Vorherige/nächste Seite Hauptmenü Veröffentlicht: %s. Lesefortschrittsleiste Ergebnisse für „%s“ Zweitmenü Seitenleiste Tag: %s Tags: %s Es gibt %d Ergebnisse für Ihre Suche. Es gibt keine Ergebnisse für Ihre Suche. Es gibt 1 Ergebnis für Ihre Suche. Das ist eine Top-Level-Seite ohne übergeordnete Seiten. Deshalb kann die Leiste auf dieser Seite nicht angezeigt werden. Diese Seite existiert nicht. Zurückkehren zur %s? Beitragsbild (Einstellungen) Titel Obere Seitenleiste (Seiten) Obere Seitenleiste (Posts) Schriftfarbe von besuchten Links vor Inhalt Startseite in Seitenleiste neben Titel Seiten weiterlesen wählen 