<?php
/**
 * Template part for displaying the next-previous bar.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php $pages = antique_theme_get_next_previous_pages(); ?>
<?php $previous_page = $pages['previous']; ?>
<?php $next_page = $pages['next']; ?>

<?php if ($previous_page || $next_page) : ?>

    <div id="next-previous-bar" class="post-navigation-bar no-select">
        <div class="wrapper">
            <div class="next-previous-arrows-wrap arrows-wrap">
                <div class="next-previous-arrow previous-arrow arrow">

                    <?php if ($previous_page) : ?>
                        <span class="next-previous-title arrow-post-title"><?php
                            echo antique_theme_truncate_string(
                                    string: get_the_title(post: $previous_page->ID),
                                    length: 42
                            );
                            ?></span>
                        <a class="next-previous-link arrow-post-link"
                           href="<?php echo get_permalink(post: $previous_page->ID); ?>">
                        </a>
                    <?php endif; ?>

                </div>
                <div class="next-previous-arrow next-arrow arrow">

                    <?php if ($next_page) : ?>
                        <span class="next-previous-title arrow-post-title"><?php
                            echo antique_theme_truncate_string(
                                    string: get_the_title(post: $next_page->ID),
                                    length: 42
                            );
                            ?></span>
                        <a class="arrow-post-link"
                           href="<?php echo get_permalink(post: $next_page->ID); ?>">
                        </a>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>

<?php endif; ?>