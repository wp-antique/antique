<?php
/**
 * Template part for displaying the reading progress bar.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<progress id="reading-progress-bar" value="0"></progress>