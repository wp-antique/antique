<?php
/**
 * Template part for displaying the site navigation opener on mobile devices.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<div class="mobile-nav-opener mobile-nav-toggle">
    <div class="wrapper">

        <?php get_template_part('template-parts/header/site-branding'); ?>

        <div class="mobile-nav-opener-icon">
            <div class="icon-bar-top"></div>
            <div class="icon-bar-middle"></div>
            <div class="icon-bar-bottom"></div>
        </div>
        
    </div>
</div>

<div class="mobile-dark-layer mobile-nav-toggle"></div>

<div id="mobile-to-top">
    <i class="fa-solid fa-angle-up"></i>
</div>