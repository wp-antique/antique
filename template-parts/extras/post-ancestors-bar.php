<?php
/**
 * The template part for displaying the post ancestors bar.
 *
 * @package Antique
 * @since Antique 1.0
 */

$ancestors = antique_theme_get_post_ancestors();

if ($ancestors) :
    ?>

    <div id="post-ancestors-bar" class="post-navigation-bar no-select">
        <div class="wrapper">
            <div class="post-ancestors-arrows-wrap arrows-wrap">

                <?php foreach ($ancestors as $ancestor) : ?>
                    <div class="post-ancestors-arrow arrow">

                        <span class="post-ancestors-title arrow-post-title"><?php
                    echo antique_theme_truncate_string(
                            string: get_the_title(post: $ancestor),
                            length: 36
                    );
                    ?></span>
                        <a class="arrow-post-link"
                           href="<?php echo get_permalink(post: $ancestor); ?>">
                        </a>

                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>

<?php endif; ?>