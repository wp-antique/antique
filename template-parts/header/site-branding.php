<?php
/**
 * Template part for displaying the site branding.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<div class="site-branding">
    <?php if (has_custom_logo()) : ?>
        <div class="site-logo"><?php the_custom_logo(); ?></div>
    <?php endif; ?>
</div>