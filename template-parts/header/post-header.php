<?php
/**
 * Template part for displaying the post header.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<header id="page-header" class="site-page-header">
    <div class="wrapper">
        <div class="page-header-inner-wrap">
            <div class="page-title-wrap">
                <h1 class="page-title"><?php echo esc_html(get_the_title()); ?></h1>
            </div>
            <?php get_template_part('template-parts/thumbnail/thumbnail-next-to-title'); ?>
        </div>
    </div>
</header>
