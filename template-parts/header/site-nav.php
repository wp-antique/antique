<?php
/**
 * Template part for displaying the site navigation.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php if (has_nav_menu('primary')) : ?>

    <nav id="site-navigation">
        <?php
        wp_nav_menu(
                array(
                    'theme_location' => 'primary',
                    'container' => 'div',
                    'container_class' => 'nav-bar'
                )
        );
        ?>
    </nav>

<?php endif; ?>