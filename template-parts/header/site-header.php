<?php
/**
 * Template part for displaying the site header.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<header id="masthead" class="site-header">
    <div class="wrapper">
        <div class="site-header-inner-wrap">
            <?php get_template_part('template-parts/header/site-branding'); ?>
            <?php get_template_part('template-parts/header/site-nav'); ?>
        </div>
    </div>
</header>