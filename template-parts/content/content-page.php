<?php
/**
 * Template part for displaying the page content in page.php.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-page
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<div id="page-content-area" class="site-page-content-area">
    <div class="wrapper">
        <div class="site-page-content adjust-overflow">

            <?php get_template_part('template-parts/thumbnail/thumbnail-before-content'); ?>

            <div class="post-content-wrap">

                <article id="post-<?php the_ID(); ?>" <?php post_class('post-content'); ?>>
                    <?php the_content(); ?>
                    <?php antique_theme_post_pagination(); ?>
                </article>

            </div>

            <?php get_sidebar('sidebar'); ?>

        </div>
    </div>
</div>