<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php get_search_form(); ?>