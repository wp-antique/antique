<?php
/**
 * Template part for displaying the thumbnail before the post content.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php $tn_settings = antique_theme_get_thumbnail_settings(); ?>

<?php if (has_post_thumbnail()): ?>
    <?php if ($tn_settings['position'] == 'before-content') : ?>

        <div class="thumbnail-before-content-wrap"
             style="aspect-ratio: <?php echo $tn_settings['aspect-ratio']; ?>">

            <?php
            the_post_thumbnail(
                    size: 'large',
                    attr: array(
                        'class' => 'thumbnail thumbnail-before-content',
                    )
            );
            ?>

            <?php if ($tn_settings['caption'] != ''): ?>
                <span class="thumbnail-caption"><?php
                    echo $tn_settings['caption'];
                    ?></span>
            <?php endif; ?>

        </div>

    <?php endif; ?>
<?php endif; ?>