<?php
/**
 * Template part for displaying the thumbnail in the sidebar.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php $tn_settings = antique_theme_get_thumbnail_settings(); ?>

<?php if (has_post_thumbnail()): ?>
    <?php if ($tn_settings['position'] == 'in-sidebar') : ?>

        <div class="thumbnail-in-sidebar-wrap"
             style="aspect-ratio: <?php echo $tn_settings['aspect-ratio']; ?>">

            <?php
            the_post_thumbnail(
                    size: 'medium_large',
                    attr: array(
                        'class' => 'thumbnail thumbnail-in-sidebar',
                    )
            );
            ?>

            <?php if ($tn_settings['caption'] != ''): ?>
                <span class="thumbnail-caption"><?php
                    echo $tn_settings['caption'];
                    ?></span>
            <?php endif; ?>

        </div>

    <?php endif; ?>
<?php endif; ?>