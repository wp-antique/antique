<?php
/**
 * Template part for displaying the thumbnail next to the post title.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php $tn_settings = antique_theme_get_thumbnail_settings(); ?>

<?php if (has_post_thumbnail()): ?>
    <?php if ($tn_settings['position'] == 'next-to-title') : ?>

        <?php

        the_post_thumbnail(
                size: 'medium',
                attr: array(
                    'class' => 'thumbnail thumbnail-next-to-title'
                )
        );
        ?>

    <?php endif; ?>
<?php endif; ?>