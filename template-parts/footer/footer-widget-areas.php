<?php
/**
 * Template part for displaying the widget areas in the footer.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php if (antique_theme_is_active_footer_widget_areas()): ?>

    <div class="footer-widget-areas-wrap">

        <?php if (is_active_sidebar('footer_widget_area_1')): ?>
            <aside id="footer-widget-area-1" class="footer-widget-area">
                <?php dynamic_sidebar('footer_widget_area_1'); ?>
            </aside>
        <?php endif; ?>

        <?php if (is_active_sidebar('footer_widget_area_2')): ?>
            <aside id="footer-widget-area-2" class="footer-widget-area">
                <?php dynamic_sidebar('footer_widget_area_2'); ?>
            </aside>
        <?php endif; ?>

        <?php if (is_active_sidebar('footer_widget_area_3')): ?>
            <aside id="footer-widget-area-3" class="footer-widget-area">
                <?php dynamic_sidebar('footer_widget_area_3'); ?>
            </aside>
        <?php endif; ?>

        <?php if (is_active_sidebar('footer_widget_area_4')): ?>
            <aside id="footer-widget-area-4" class="footer-widget-area">
                <?php dynamic_sidebar('footer_widget_area_4'); ?>
            </aside>
        <?php endif; ?>

    </div>

<?php endif; ?>