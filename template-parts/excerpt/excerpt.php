<?php
/**
 * Template part for displaying a post excerpt.
 *
 * @package Antique
 * @since Antique 1.0
 */

if ('page' == get_post_type()) {
    get_template_part('template-parts/excerpt/excerpt-page');
} else if ('post' == get_post_type()) {
    get_template_part('template-parts/excerpt/excerpt-single');
}