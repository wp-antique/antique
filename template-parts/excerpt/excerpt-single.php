<?php
/**
 * Template part for displaying a single post excerpt in
 * /template-parts/excerpt/excerpt.php.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<article id="post-<?php esc_attr(the_ID()); ?>" class="excerpt is-post">

    <div class="post-meta-top">
        <?php antique_theme_post_author(); ?>
        <?php antique_theme_post_date(); ?>
    </div>

    <header class="excerpt-header">
        <?php
        the_title(
                before: sprintf(
                        '<h2 class="excerpt-title">'
                        . '<a href="%s" class="excerpt-link">',
                        esc_url(get_permalink())
                ),
                after: '</a></h2>'
        );
        ?>
    </header>

    <div class="excerpt-content">
        <?php the_excerpt(); ?>
    </div>

    <div class="post-meta-bottom">
        <?php antique_theme_single_taxonomies(); ?>
    </div>

</article>