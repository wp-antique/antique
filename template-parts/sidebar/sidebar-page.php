<?php
/**
 * Template part for displaying the sidebars on pages.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php if (antique_theme_is_active_top_sidebar(post_type: 'page')) : ?>

    <aside id="top-sidebar"
           class="sidebar">

        <?php
        $tn_settings = antique_theme_get_thumbnail_settings();

        if (has_post_thumbnail() && $tn_settings['position'] == 'next-to-title') {
            if (!empty($tn_settings['caption'])) {
                printf(
                        '<section class="widget">'
                        . '<span class="thumbnail-caption">*%s</span>'
                        . '</section>',
                        $tn_settings['caption']
                );
            }
        }

        if (has_post_thumbnail() && $tn_settings['position'] == 'in-sidebar') {
            get_template_part('template-parts/thumbnail/thumbnail-in-sidebar');
        }

        if (antique_theme_is_active_sidebar_blocks()) {
            antique_theme_display_sidebar_blocks();
        }

        if (is_active_sidebar('pages_top_sidebar')) {
            dynamic_sidebar('pages_top_sidebar');
        }
        ?>

    </aside>

<?php endif; ?>
<?php if (is_active_sidebar('pages_bottom_sidebar')) : ?>

    <aside id="bottom-sidebar"
           class="sidebar">

        <?php dynamic_sidebar('pages_bottom_sidebar'); ?>

    </aside>

<?php endif; ?>