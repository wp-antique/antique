<?php
/**
 * Template part for displaying the sidebars on single posts.
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php if (antique_theme_is_active_top_sidebar(post_type: 'post')) : ?>

    <aside id="top-sidebar"
           class="sidebar">

        <?php
        $tn_settings = antique_theme_get_thumbnail_settings();

        if (has_post_thumbnail() && $tn_settings['position'] == 'next-to-title') {
            if (!empty($tn_settings['caption'])) {
                printf(
                        '<span class="thumbnail-caption">*%s</span>',
                        $tn_settings['caption']
                );
            }
        }

        if (has_post_thumbnail() && $tn_settings['position'] == 'in-sidebar') {
            get_template_part('template-parts/thumbnail/thumbnail-in-sidebar');
        }

        if (antique_theme_is_active_sidebar_blocks()) {
            antique_theme_display_sidebar_blocks();
        }

        if (is_active_sidebar('posts_top_sidebar')) {
            dynamic_sidebar('posts_top_sidebar');
        }
        ?>

    </aside>

<?php endif; ?>
<?php if (is_active_sidebar('posts_bottom_sidebar')) : ?>

    <aside id="bottom-sidebar"
           class="sidebar">

        <?php dynamic_sidebar('posts_bottom_sidebar'); ?>

    </aside>

<?php endif; ?>