<?php
/**
 * The template for displaying the blog posts index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#home-page-display
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php get_header(); ?>

<div id="page-content-area" class="site-page-content-area">
    <div class="wrapper adjust-overflow">
        <div class="site-page-content">

            <?php
            if (have_posts()) {

                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/excerpt/excerpt');
                }

                antique_theme_posts_pagination();
            } else {
                get_template_part('template-parts/content/content-none');
            }
            ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>