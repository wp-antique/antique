<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php get_header(); ?>

<header id="page-header" class="site-page-header">
    <div class="wrapper">
        <div class="page-header-inner-wrap">

            <div class="page-title-wrap">
                <h1 class="page-title"><?php
                    esc_html_e('Error 404', 'antique');
                    ?></h1>
                <span class="page-subtitle"><?php
                    esc_html_e('Page not found', 'antique');
                    ?></span>
            </div>

        </div>
    </div>
</header>

<div id="page-content-area" class="site-page-content-area">
    <div class="wrapper adjust-overflow">
        <div class="site-page-content">

            <p><?php
                printf(
                        esc_html__(
                                'This page does not exist. Return to %s?',
                                'antique'
                        ),
                        sprintf(
                                '<a href="%s">%s</a>',
                                esc_url(get_home_url(get_the_ID())),
                                esc_html__('homepage', 'antique')
                        )
                );
                ?></p>

            <?php get_search_form(); ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>