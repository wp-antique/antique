<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (besides style.css). If WordPress cannot
 * find a template file with matching name or any template file at all, the
 * theme will use index.php.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#overview
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php get_header(); ?>

<div id="page-content-area" class="site-page-content-area">
    <div class="wrapper adjust-overflow">
        <div class="site-page-content">

            <?php
            if (have_posts()) {

                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/excerpt/excerpt');
                }

                antique_theme_posts_pagination();
            } else {
                get_template_part('template-parts/content/content-none');
            }
            ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>