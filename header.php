<?php
/**
 * The template for displaying the header.
 *
 * It displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php wp_head(); ?>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    </head>

    <body <?php body_class(); ?>>

        <?php get_template_part('template-parts/extras/progress-bar'); ?>
        <?php get_template_part('template-parts/header/site-header'); ?>

        <main id="main" class="site-main">