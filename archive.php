<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Creating_an_Archive_Index
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#category
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#tag
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#author-display
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#author-display
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

<?php get_header(); ?>

<header id="page-header" class="site-page-header">
    <div class="wrapper">
        <div class="page-header-inner-wrap">
            <div class="page-title-wrap">

                <h1 class="page-title"><?php
                    if (is_category()) {
                        printf(
                                __('Category: %s', 'antique'),
                                esc_html(single_cat_title(display: false))
                        );
                    } else if (is_tag()) {
                        printf(
                                __('Tag: %s', 'antique'),
                                esc_html(single_tag_title(display: false))
                        );
                    } else if (is_date()) {
                        printf(
                                __('Posts from %s', 'antique'),
                                get_the_date(format: 'F Y')
                        );
                    } else if (is_author()) {
                        printf(
                                __('Posts by %s', 'antique'),
                                esc_html(antique_theme_current_author())
                        );
                    } else {
                        esc_html_e('Archive', 'antique');
                    }
                    ?></h1>

            </div>
        </div>
    </div>
</header>

<div id="page-content-area" class="site-page-content-area">
    <div class="wrapper adjust-overflow">
        <div class="site-page-content">

            <?php
            if (is_author()) {
                antique_theme_about_author();
            }
            ?>

            <?php
            if (have_posts()) {

                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/excerpt/excerpt');
                }

                antique_theme_posts_pagination();
            } else {
                get_template_part('template-parts/content/content-none');
            }
            ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>