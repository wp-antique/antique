<?php
/**
 * Functions related to the meta box for Antique blocks in the sidebar.
 *
 * @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
 *
 * @package Antique
 * @since Antique 1.0
 */

function antique_theme_sidebar_blocks_get_post_meta($id) {

    $defaults = antique_theme_sidebar_blocks_get_meta_defaults();

    $meta_key = antique_theme_sidebar_blocks_get_meta_strings()['key'];
    $saved_meta = get_post_meta(
            post_id: $id,
            key: $meta_key,
            single: false
    );

    $full_meta = antique_theme_parse_meta_args(
            args: isset($saved_meta[0]) ? $saved_meta[0] : array(),
            defaults: $defaults
    );

    return $full_meta;
}

if (!function_exists('antique_theme_sidebar_blocks_get_meta_defaults')) {

    /**
     * Returns the default meta values.
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_sidebar_blocks_get_meta_defaults() {

        $plugins = antique_plugins_get_info();
        $defaults = array();

        foreach ($plugins as $info) {
            if ($info['is-active'] && $info['has-sidebar-meta']) {
                $defaults[$info['domain']] = $info['sidebar_args'];
            }
        }

        return $defaults;
    }

}

if (!function_exists('antique_theme_sidebar_blocks_get_meta_strings')) {

    /**
     * Returns strings used for meta key, meta fields and meta nonce.
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_sidebar_blocks_get_meta_strings() {

        $meta_labels = array(
            'field' => 'antique_theme_sidebar_blocks_meta_field',
            'key' => '_antique_theme_sidebar_blocks_meta_key',
            'nonce_name' => 'antique_theme_sidebar_blocks_meta_nonce',
            'nonce_action' => 'add_antique_theme_sidebar_blocks_meta_nonce',
        );

        return $meta_labels;
    }

}

add_action(
        hook_name: 'add_meta_boxes',
        callback: 'antique_theme_sidebar_blocks_meta_box'
);

function antique_theme_sidebar_blocks_meta_box() {

    if (!function_exists('antique_plugins_get_info')) {
        return;
    }

    $plugins = antique_plugins_get_info();
    $are_active = array();

    foreach ($plugins as $info) {
        if ($info['has-sidebar-meta']) {
            $are_active[] = $info['is-active'];
        }
    }

    if (in_array(true, $are_active, true)) {

        add_meta_box(
                id: 'antique_theme_sidebar_blocks',
                title: __('Sidebar', 'antique'),
                callback: 'antique_theme_sidebar_blocks_meta_box_html',
                screen: 'page',
                context: 'side',
                priority: 'high'
        );
    }
}

function antique_theme_sidebar_blocks_meta_box_html($post) {

    $meta = antique_theme_sidebar_blocks_get_meta_strings();

    $meta_field = $meta['field'];
    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    wp_nonce_field(
            action: $nonce_action,
            name: $nonce_name
    );

    $post_meta = antique_theme_sidebar_blocks_get_post_meta(
            id: $post->ID
    );

    $plugins = antique_plugins_get_info();

    foreach ($plugins as $plugin) {

        if (!$plugin['is-active'] || !$plugin['has-sidebar-meta']) {
            continue;
        }

        $values = $post_meta[$plugin['domain']];

        foreach (array_keys($plugin['sidebar_args']) as $arg) {

            $block_field = sprintf(
                    '%s[%s][%s]',
                    $meta_field,
                    $plugin['domain'],
                    $arg
            );
            $is_display_checked = ($values[$arg] == 'on') ? 'checked' : '';
            ?>

            <div class="components-panel__row">
                <label for="<?php echo esc_attr($block_field); ?>"
                       ><?php echo $plugin['heading']; ?>:</label>
                <input type="checkbox"
                       id="<?php echo esc_attr($block_field); ?>"
                       name="<?php echo esc_attr($block_field); ?>"
                       style="margin: auto 0;"
                       <?php echo $is_display_checked; ?>
                       autocomplete="off"
                       >
            </div>

            <?php
        }
    }
}

add_action(
        hook_name: 'save_post',
        callback: 'antique_theme_sidebar_blocks_save_meta'
);

function antique_theme_sidebar_blocks_save_meta($post_id) {

    $meta = antique_theme_sidebar_blocks_get_meta_strings();

    $meta_field = $meta['field'];
    $meta_key = $meta['key'];
    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    if (!isset($_POST[$nonce_name])) {
        return;
    }

    if (!wp_verify_nonce(
                    nonce: $_POST[$nonce_name],
                    action: $nonce_action
            )) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return;
        }
    }

    $sanitized = antique_theme_sidebar_blocks_sanitize_meta(
            post_meta: isset($_POST[$meta_field]) ? $_POST[$meta_field] : array()
    );

    update_post_meta(
            post_id: $post_id,
            meta_key: $meta_key,
            meta_value: $sanitized
    );
}

function antique_theme_sidebar_blocks_sanitize_meta($post_meta) {

    $defaults = antique_theme_sidebar_blocks_get_meta_defaults();
    $values = antique_theme_parse_meta_args(
            args: $post_meta,
            defaults: $defaults
    );

    return $values;
}
