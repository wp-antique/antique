<?php
/**
 * Functions related to the thumbnail meta box.
 *
 * @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
 *
 * @package Antique
 * @since Antique 1.0
 */

function antique_theme_thumbnail_get_post_meta($id) {

    $defaults = antique_theme_thumbnail_get_meta_defaults();

    $meta_key = antique_theme_thumbnail_get_meta_strings()['key'];
    $saved_meta = get_post_meta(
            post_id: $id,
            key: $meta_key,
            single: false
    );

    $full_meta = antique_theme_parse_meta_args(
            args: isset($saved_meta[0]) ? $saved_meta[0] : array(),
            defaults: $defaults
    );

    return $full_meta;
}

if (!function_exists('antique_theme_thumbnail_get_meta_defaults')) {

    /**
     * Returns the default meta values.
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_thumbnail_get_meta_defaults() {

        $defaults = array(
            'position' => 'in-sidebar',
            'aspect-ratio' => array(
                'in-sidebar' => array(
                    'x' => 3,
                    'y' => 4,
                ),
                'before-content' => array(
                    'x' => 4,
                    'y' => 1,
                )
            ),
            'caption' => '',
        );

        return $defaults;
    }

}

if (!function_exists('antique_theme_thumbnail_get_meta_strings')) {

    /**
     * Returns strings used for meta key, meta fields and meta nonce.
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_thumbnail_get_meta_strings() {

        $meta_labels = array(
            'field' => 'antique_theme_thumbnail_meta_field',
            'key' => '_antique_theme_thumbnail_meta_key',
            'nonce_name' => 'antique_theme_thumbnail_meta_nonce',
            'nonce_action' => 'add_antique_theme_thumbnail_meta_nonce',
        );

        return $meta_labels;
    }

}

add_action(
        hook_name: 'add_meta_boxes',
        callback: 'antique_theme_thumbnail_meta_box'
);

function antique_theme_thumbnail_meta_box() {

    add_meta_box(
            id: 'antique_theme_thumbnail',
            title: __('Thumbnail (settings)', 'antique'),
            callback: 'antique_theme_thumbnail_meta_box_html',
            screen: array(
                'page',
                'post',
            ),
            context: 'side',
            priority: 'high'
    );
}

function antique_theme_thumbnail_meta_box_html($post) {

    $meta = antique_theme_thumbnail_get_meta_strings();

    $meta_field = $meta['field'];
    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    wp_nonce_field(
            action: $nonce_action,
            name: $nonce_name
    );

    $post_meta = antique_theme_thumbnail_get_post_meta(
            id: $post->ID
    );

    $position = $post_meta['position'];
    $aspect_ratio = $post_meta['aspect-ratio'];
    $caption = $post_meta['caption'];
    ?>

    <script>
        var $ = jQuery.noConflict();

        $(document).ready(function () {

            function check_thumbnail_meta() {

                if (tn_position_opt.value === 'next-to-title') {

                    tn_ar_sidebar_wrap.style.display = 'none';
                    tn_ar_content_wrap.style.display = 'none';
                    tn_ar_expl_wrap.style.display = 'none';

                } else if (tn_position_opt.value === 'before-content') {

                    tn_ar_sidebar_wrap.style.display = 'none';
                    tn_ar_content_wrap.style.display = 'block';
                    tn_ar_expl_wrap.style.display = 'block';

                } else if (tn_position_opt.value === 'in-sidebar') {

                    tn_ar_sidebar_wrap.style.display = 'block';
                    tn_ar_content_wrap.style.display = 'none';
                    tn_ar_expl_wrap.style.display = 'block';
                }
            }

            var tn_position_opt = document.getElementById(
                    '<?php echo esc_attr($meta_field); ?>[position]'
                    );

            var tn_ar_sidebar_wrap = document.getElementById(
                    'thumbnail-meta-ar-sidebar-wrap'
                    );
            var tn_ar_content_wrap = document.getElementById(
                    'thumbnail-meta-ar-content-wrap'
                    );
            var tn_ar_expl_wrap = document.getElementById(
                    'thumbnail-meta-ar-expl-wrap'
                    );

            check_thumbnail_meta();

            tn_position_opt.addEventListener('change', function () {
                check_thumbnail_meta();
            });

        });
    </script>

    <div class="components-panel__row">
        <label for="<?php echo esc_attr($meta_field); ?>[position]"><?php
            esc_html_e('Position:', 'antique');
            ?></label>

        <select id="<?php echo esc_attr($meta_field); ?>[position]"
                name="<?php echo esc_attr($meta_field); ?>[position]"
                style="margin: auto 0;"
                autocomplete="off"
                >

            <option value="next-to-title"
            <?php echo $position == 'next-to-title' ? 'selected' : '' ?>
                    ><?php esc_html_e('next to title', 'antique'); ?></option>
            <option value="before-content"
            <?php echo $position == 'before-content' ? 'selected' : '' ?>
                    ><?php esc_html_e('before content', 'antique'); ?></option>
            <option value="in-sidebar"
            <?php echo $position == 'in-sidebar' ? 'selected' : '' ?>
                    ><?php esc_html_e('in sidebar', 'antique'); ?></option>

        </select>
    </div>

    <div id="thumbnail-meta-ar-sidebar-wrap" class="thumbnail-meta-wrap">
        <div class="components-panel__row"
             style="margin-bottom: -16px">
            <label for="<?php echo esc_attr($meta_field); ?>[aspect-ratio][in-sidebar][x]"><?php
                esc_html_e('Aspect ratio:', 'antique');
                ?></label>
        </div>

        <div class="components-panel__row">
            <input type="number"
                   id="<?php echo esc_attr($meta_field); ?>[aspect-ratio][in-sidebar][x]"
                   name="<?php echo esc_attr($meta_field); ?>[aspect-ratio][in-sidebar][x]"
                   value="<?php echo $aspect_ratio['in-sidebar']['x'] ?>"
                   min="1" max="32" step="1"
                   autocomplete="off"
                   >
            <span>/</span>
            <input type="number"
                   id="<?php echo esc_attr($meta_field); ?>[aspect-ratio][in-sidebar][y]"
                   name="<?php echo esc_attr($meta_field); ?>[aspect-ratio][in-sidebar][y]"
                   value="<?php echo $aspect_ratio['in-sidebar']['y'] ?>"
                   min="1" max="32" step="1"
                   autocomplete="off"
                   >
        </div>
    </div>

    <div id="thumbnail-meta-ar-content-wrap" class="thumbnail-meta-wrap">
        <div class="components-panel__row"
             style="margin-bottom: -16px">
            <label for="<?php echo esc_attr($meta_field); ?>[aspect-ratio][before-content][x]"><?php
                esc_html_e('Aspect ratio:', 'antique');
                ?></label>
        </div>
        <div class="components-panel__row">
            <input type="number"
                   id="<?php echo esc_attr($meta_field); ?>[aspect-ratio][before-content][x]"
                   name="<?php echo esc_attr($meta_field); ?>[aspect-ratio][before-content][x]"
                   value="<?php echo $aspect_ratio['before-content']['x'] ?>"
                   min="1" max="32" step="1"
                   autocomplete="off"
                   >
            <span>/</span>
            <input type="number"
                   id="<?php echo esc_attr($meta_field); ?>[aspect-ratio][before-content][y]"
                   name="<?php echo esc_attr($meta_field); ?>[aspect-ratio][before-content][y]"
                   value="<?php echo $aspect_ratio['before-content']['y'] ?>"
                   min="1" max="32" step="1"
                   autocomplete="off"
                   >
        </div>
    </div>

    <div id="thumbnail-meta-ar-expl-wrap" class="meta-explanation-wrap">
        <p class="meta-explanation"><?php
            esc_html_e('For thumbnails displayed in the sidebar it is '
                    . 'recommended to use an aspect ratio of 3/4, 2/3 or '
                    . '1/1, for thumbnails displayed below the title 4/1 '
                    . 'or 5/1.', 'antique');
            ?></p>
    </div>

    <div id="thumbnail-meta-caption-wrap" class="thumbnail-meta-wrap">
        <div class="components-panel__row"
             style="margin-bottom: -16px">
            <label for="<?php echo esc_attr($meta_field); ?>[caption]"><?php
                esc_html_e('Caption:', 'antique');
                ?></label>
        </div>

        <div class="components-panel__row">
            <textarea id="<?php echo esc_attr($meta_field); ?>[caption]"
                      name="<?php echo esc_attr($meta_field); ?>[caption]"
                      style="width: 100%; margin: 0;"
                      rows=1
                      autocomplete="off"
                      ><?php echo $caption; ?></textarea>
        </div>
    </div>

    <?php
}

add_action(
        hook_name: 'save_post',
        callback: 'antique_theme_thumbnail_save_meta'
);

function antique_theme_thumbnail_save_meta($post_id) {

    $meta = antique_theme_thumbnail_get_meta_strings();

    $meta_field = $meta['field'];
    $meta_key = $meta['key'];

    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    if (!isset($_POST[$nonce_name])) {
        return;
    }

    if (!wp_verify_nonce(
                    nonce: $_POST[$nonce_name],
                    action: $nonce_action
            )) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (
            isset($_POST['post_type']) && (
            'page' == $_POST['post_type'] || 'post' == $_POST['post_type']
            )
    ) {
        if (!current_user_can('edit_page', $post_id)) {
            return;
        }
    }

    if (!isset($_POST[$meta_field])) {
        return;
    }

    $sanitized = antique_theme_thumbnail_sanitize_meta(
            post_meta: $_POST[$meta_field]
    );

    update_post_meta(
            post_id: $post_id,
            meta_key: $meta_key,
            meta_value: $sanitized
    );
}

function antique_theme_thumbnail_sanitize_meta($post_meta) {

    $allowed_positions = array(
        'next-to-title',
        'before-content',
        'in-sidebar',
    );

    $allowed_html = array(
        'a' => array(
            'href' => array(),
            'target' => array()
        ),
        'b' => array(),
        'strong' => array(),
        'em' => array(),
        'br' => array()
    );

    $defaults = antique_theme_thumbnail_get_meta_defaults();
    $values = antique_theme_parse_meta_args(
            args: $post_meta,
            defaults: $defaults
    );

    $sanitized = $defaults;

    // Sanitize thumbnail position
    if (in_array(
                    needle: $values['position'],
                    haystack: $allowed_positions
            )) {
        $sanitized['position'] = sanitize_text_field(str: $values['position']);
    }

    // Sanitize thumbnail aspect ratio
    foreach (array('before-content', 'in-sidebar') as $position) {

        $x_val = $values['aspect-ratio'][$position]['x'];
        $y_val = $values['aspect-ratio'][$position]['y'];

        if (is_numeric($x_val) && 1 <= $x_val && $x_val <= 32) {
            $sanitized['aspect-ratio'][$position]['x'] = (int) $x_val;
        }

        if (is_numeric($y_val) && 1 <= $y_val && $y_val <= 32) {
            $sanitized['aspect-ratio'][$position]['y'] = (int) $y_val;
        }
    }

    // Sanitize thumbnail caption
    $sanitized['caption'] = wp_kses(
            content: $values['caption'],
            allowed_html: $allowed_html
    );

    return $sanitized;
}
