<?php
/**
 * Functions related to the meta box for page navigation bars.
 *
 * @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
 *
 * @package Antique
 * @since Antique 1.0
 */
function antique_theme_post_navs_get_post_meta($id) {

    $defaults = antique_theme_post_navs_get_meta_defaults();

    $meta_key = antique_theme_post_navs_get_meta_strings()['key'];
    $saved_meta = get_post_meta(
            post_id: $id,
            key: $meta_key,
            single: false
    );

    $full_meta = antique_theme_parse_meta_args(
            args: isset($saved_meta[0]) ? $saved_meta[0] : array(),
            defaults: $defaults
    );

    return $full_meta;
}

if (!function_exists('antique_theme_post_navs_get_meta_defaults')) {

    /**
     * Returns the default meta values.
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_post_navs_get_meta_defaults() {

        $defaults = array(
            'ancestors-bar' => array(
                'display' => 'on',
                'number' => 3,
            ),
            'next-previous-bar' => array(
                'display' => '',
                'previous' => '',
                'next' => ''
            )
        );

        return $defaults;
    }

}

if (!function_exists('antique_theme_post_navs_get_meta_strings')) {

    /**
     * Returns strings used for meta key, meta fields and meta nonce.
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_post_navs_get_meta_strings() {

        $meta_labels = array(
            'field' => 'antique_theme_post_navs_meta_field',
            'key' => '_antique_theme_post_navs_meta_key',
            'nonce_name' => 'antique_theme_post_navs_meta_nonce',
            'nonce_action' => 'add_antique_theme_post_navs_meta_nonce',
        );

        return $meta_labels;
    }

}

add_action(
        hook_name: 'add_meta_boxes',
        callback: 'antique_theme_post_navs_meta_box'
);

function antique_theme_post_navs_meta_box() {

    add_meta_box(
            id: 'antique_theme_post_navs',
            title: __('Navigation bars', 'antique'),
            callback: 'antique_theme_post_navs_meta_box_html',
            screen: 'page',
            context: 'side',
            priority: 'high'
    );
}

function antique_theme_post_navs_meta_box_html($post) {

    $meta = antique_theme_post_navs_get_meta_strings();

    $meta_field = $meta['field'];
    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    wp_nonce_field(
            action: $nonce_action,
            name: $nonce_name
    );

    $post_meta = antique_theme_post_navs_get_post_meta(
            id: $post->ID
    );

    $is_single = get_post_type($post) == 'post';
    $is_ancestor_bar_checked = ($post_meta['ancestors-bar']['display'] == 'on') ? 'checked' : '';
    $is_np_bar_checked = ($post_meta['next-previous-bar']['display'] == 'on') ? 'checked' : '';

    $parent = get_post_parent(post: $post);

    $dropdown_previous_args = array(
        'depth' => 1,
        'child_of' => $parent ? $parent->ID : 0,
        'name' => esc_attr($meta_field) . '[next-previous-bar][previous]',
        'id' => esc_attr($meta_field) . '[next-previous-bar][previous]',
        'selected' => $post_meta['next-previous-bar']['previous'],
        'option_none_value' => '',
        'show_option_none' => sprintf(
                '--- %s ---',
                esc_html__('select', 'antique')
        ),
        'exclude' => array($post->ID),
    );

    $dropdown_next_args = array(
        'depth' => 1,
        'child_of' => $parent ? $parent->ID : 0,
        'name' => esc_attr($meta_field) . '[next-previous-bar][next]',
        'id' => esc_attr($meta_field) . '[next-previous-bar][next]',
        'selected' => $post_meta['next-previous-bar']['next'],
        'option_none_value' => '',
        'show_option_none' => sprintf(
                '--- %s ---',
                esc_html__('select', 'antique')
        ),
        'exclude' => array($post->ID),
    );
    ?>

    <div class="components-panel__row">
        <p class="meta-heading"><?php
            esc_html_e('Parent pages', 'antique');
            ?></p>
    </div>

    <?php if ($parent) : ?>

        <div class="components-panel__row">
            <label for="<?php echo esc_attr($meta_field); ?>[ancestors-bar][display]"><?php
                esc_html_e('Display bar:', 'antique');
                ?></label>
            <input type="checkbox"
                   id="<?php echo esc_attr($meta_field); ?>[ancestors-bar][display]"
                   name="<?php echo esc_attr($meta_field); ?>[ancestors-bar][display]"
                   style="margin: auto 0;"
                   <?php echo $is_ancestor_bar_checked; ?>
                   autocomplete="off"
                   >
        </div>

        <div class="components-panel__row">
            <label for="<?php echo esc_attr($meta_field); ?>[ancestors-bar][number]"><?php
                esc_html_e('Number:', 'antique');
                ?></label>
            <input type="number"
                   id="<?php echo esc_attr($meta_field); ?>[ancestors-bar][number]"
                   name="<?php echo esc_attr($meta_field); ?>[ancestors-bar][number]"
                   value="<?php echo esc_attr($post_meta['ancestors-bar']['number']); ?>"
                   min="1" max="6" step="1"
                   style="width: 60px"
                   autocomplete="off"
                   >
        </div>

    <?php else : ?>

        <div class="components-panel__row">
            <p class="meta-explanation"><?php
                esc_html_e("This is a top-level page and doesn't have any "
                        . "parent pages. Therefore, the bar cannot be "
                        . "displayed on this page.", 'antique');
                ?></p>
        </div>

    <?php endif; ?>

    <div class="components-panel__row">
        <p class="meta-heading"><?php
            esc_html_e('Previous/next page', 'antique');
            ?></p>
    </div>

    <div class="components-panel__row">
        <label for="<?php echo esc_attr($meta_field); ?>[next-previous-bar][display]"><?php
            esc_html_e('Display bar:', 'antique');
            ?></label>
        <input type="checkbox"
               id="<?php echo esc_attr($meta_field); ?>[next-previous-bar][display]"
               name="<?php echo esc_attr($meta_field); ?>[next-previous-bar][display]"
               style="margin: auto 0;"
               <?php echo $is_np_bar_checked; ?>
               autocomplete="off"
               >
    </div>

    <div class="components-panel__row panel-row-no-margin">
        <label for="<?php echo esc_attr($meta_field); ?>[next-previous-bar][previous]"><?php
            esc_html_e('Previous page:', 'antique');
            ?></label>
    </div>

    <div class="components-panel__row panel-row-no-margin">
        <?php wp_dropdown_pages(args: $dropdown_previous_args); ?>
    </div>

    <div class="components-panel__row">
        <label for="<?php echo esc_attr($meta_field); ?>[next-previous-bar][next]"><?php
            esc_html_e('Next page:', 'antique');
            ?></label>
    </div>

    <div class="components-panel__row panel-row-no-margin">
        <?php wp_dropdown_pages(args: $dropdown_next_args); ?>
    </div>

    <div class="components-panel__row">
        <p class="meta-explanation"><?php
            esc_html_e("If you don't select any page, the bar is not "
                    . "displayed.", 'antique');
            ?></p>
    </div>

    <?php
}

add_action(
        hook_name: 'save_post',
        callback: 'antique_theme_post_navs_save_meta'
);

function antique_theme_post_navs_save_meta($post_id) {

    $meta = antique_theme_post_navs_get_meta_strings();

    $meta_field = $meta['field'];
    $meta_key = $meta['key'];
    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    if (!isset($_POST[$nonce_name])) {
        return;
    }

    if (!wp_verify_nonce(
                    nonce: $_POST[$nonce_name],
                    action: $nonce_action
            )) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return;
        }
    }

    if (!isset($_POST[$meta_field])) {
        return;
    }

    $sanitized = antique_theme_post_navs_sanitize_meta(
            post_meta: $_POST[$meta_field]
    );

    update_post_meta(
            post_id: $post_id,
            meta_key: $meta_key,
            meta_value: $sanitized
    );
}

function antique_theme_post_navs_sanitize_meta($post_meta) {

    if (!isset($post_meta['ancestors-bar']['display'])) {
        $post_meta['ancestors-bar']['display'] = '';
    }

    if (!isset($post_meta['next-previous-bar']['display'])) {
        $post_meta['next-previous-bar']['display'] = '';
    }

    $all_ids = get_posts(
            args: array(
                'fields' => 'ids',
                'numberposts' => -1,
                'post_type' => array(
                    'post',
                    'page'
                ),
            )
    );

    $defaults = antique_theme_post_navs_get_meta_defaults();
    $values = antique_theme_parse_meta_args(
            args: $post_meta,
            defaults: $defaults
    );

    // --- Ancestor bar ---
    $anc_meta = $values['ancestors-bar'];
    $anc_sanitized_meta = $defaults['ancestors-bar'];

    // Sanitize checkbox
    $anc_display = isset($anc_meta['display']) ? $anc_meta['display'] : '';

    if ($anc_display == 'on') {
        $anc_sanitized_meta['display'] = 'on';
    } else {
        $anc_sanitized_meta['display'] = '';
    }

    // Sanitize number of ancestors
    $anc_number = $anc_meta['number'];

    if (is_numeric($anc_number) && 1 <= $anc_number && $anc_number <= 6) {
        $anc_sanitized_meta['number'] = (int) $anc_number;
    }

    // --- Next-previous bar ---
    $np_meta = $values['next-previous-bar'];
    $np_sanitized_meta = $defaults['next-previous-bar'];

    // Sanitize checkbox
    $np_display = isset($np_meta['display']) ? $np_meta['display'] : '';

    if ($np_display != '') {
        $np_sanitized_meta['display'] = 'on';
    } else {
        $np_sanitized_meta['display'] = '';
    }

    // Sanitize id of previous page
    $np_previous = $np_meta['previous'];
    $previous_exists = in_array(
            needle: $np_previous,
            haystack: $all_ids
    );

    if (is_numeric($np_previous) && $previous_exists) {
        $np_sanitized_meta['previous'] = (int) $np_previous;
    } else {
        $np_sanitized_meta['previous'] = '';
    }

    // Sanitize id of next page
    $np_next = $np_meta['next'];
    $next_exists = in_array(
            needle: $np_previous,
            haystack: $all_ids
    );

    if (is_numeric($np_next) && $next_exists) {
        $np_sanitized_meta['next'] = (int) $np_next;
    } else {
        $np_sanitized_meta['next'] = '';
    }

    $sanitized_meta = array(
        'ancestors-bar' => $anc_sanitized_meta,
        'next-previous-bar' => $np_sanitized_meta,
    );

    return $sanitized_meta;
}
