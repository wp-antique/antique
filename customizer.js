(function ($) {

    /*
     * Reading progress bar
     */

    wp.customize('reading_progress_bg_gradient_l_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--reading-progress--bg-color-l', new_color
                            );
                });
            });

    wp.customize('reading_progress_bg_gradient_r_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--reading-progress--bg-color-r', new_color
                            );
                });
            });

    /*
     * Header
     */

    wp.customize('header_bg_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--header--bg-color', new_color
                            );
                });
            });

    wp.customize('header_font_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--header--font-color', new_color
                            );
                });
            });

    wp.customize('header_font_color_hover_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--header--font-color--hover', new_color
                            );
                });
            });

    /*
     * Title
     */

    wp.customize('title_bg_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--title--bg-color', new_color
                            );
                });
            });

    wp.customize('title_bg_color_selection_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--title--bg-color--selection', new_color
                            );
                });
            });

    /*
     * Post navigation
     */

    wp.customize('post_nav_bg_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--post-nav--bg-color', new_color
                            );
                });
            });

    wp.customize('post_nav_font_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--post-nav--font-color', new_color
                            );
                });
            });

    /*
     * Post navigation
     */

    wp.customize('post_nav_bg_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--post-nav--bg-color', new_color
                            );
                });
            });

    wp.customize('post_nav_font_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--post-nav--font-color', new_color
                            );
                });
            });

    /*
     * Content
     */

    wp.customize('content_bg_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--content--bg-color', new_color
                            );
                });
            });

    wp.customize('content_font_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--content-font-color', new_color
                            );
                });
            });

    wp.customize('content_link_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--link--color', new_color
                            );
                });
            });

    wp.customize('content_link_visited_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--link--color-visited', new_color
                            );
                });
            });

    /*
     * Footer
     */

    wp.customize('footer_bg_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--footer--bg-color', new_color
                            );
                });
            });

    wp.customize('footer_font_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--footer--font-color', new_color
                            );
                });
            });

    wp.customize('footer_font_color_hover_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique--footer--font-color--hover', new_color
                            );
                });
            });

    /*
     * Antique blocks
     */

    wp.customize('antique_blocks_heading_bg_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique-blocks--heading--bg-color', new_color
                            );
                });
            });

    wp.customize('antique_blocks_heading_font_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique-blocks--heading--font-color', new_color
                            );
                });
            });

    wp.customize('antique_blocks_heading_border_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique-blocks--heading--border-color', new_color
                            );
                });
            });

    wp.customize('antique_blocks_content_bg_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique-blocks--content--bg-color', new_color
                            );
                });
            });

    wp.customize('antique_blocks_content_font_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique-blocks--content--font-color', new_color
                            );
                });
            });

    wp.customize('antique_blocks_content_border_color_setting',
            function (color) {
                color.bind(function (new_color) {
                    document.querySelector(':root').style.setProperty(
                            '--antique-blocks--content--border-color', new_color
                            );
                });
            });

})(jQuery);
