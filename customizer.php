<?php

/**
 * Color settings in the customizer.
 *
 * @link https://developer.wordpress.org/themes/customize-api/
 *
 * @package Antique
 * @since Antique 1.0
 */
if (!function_exists('antique_theme_customizer_default_colors')) {

    /**
     * Returns the default theme colors, which can be set in the customizer.
     *
     * @since Antique 1.0
     *
     * @return array
     */
    function antique_theme_customizer_default_colors() {

        $defaults = array(
            'reading_progress_bg_gradient_l_setting' => '#5e5e5e',
            'reading_progress_bg_gradient_r_setting' => '#ffa500',
            'header_bg_color_setting' => '#2d2d2d',
            'header_font_color_setting' => '#ffffff',
            'header_font_color_hover_setting' => '#aaaaaa',
            'title_bg_color_setting' => '#444444',
            'title_bg_color_selection_setting' => '#c8841e',
            'title_font_color_setting' => '#ffffff',
            'post_nav_bg_color_setting' => '#666666',
            'post_nav_font_color_setting' => '#ffffff',
            'content_bg_color_setting' => '#ffffff',
            'content_font_color_setting' => '#000000',
            'content_link_color_setting' => '#1c27a2',
            'content_link_visited_color_setting' => '#582da3',
            'footer_bg_color_setting' => '#2d2d2d',
            'footer_font_color_setting' => '#ffffff',
            'footer_font_color_hover_setting' => '#aaaaaa',
            'antique_blocks_heading_bg_color_setting' => '#c29a5c',
            'antique_blocks_heading_font_color_setting' => '#ffffff',
            'antique_blocks_heading_border_color_setting' => '#000000',
            'antique_blocks_content_bg_color_setting' => '#f2f2f2',
            'antique_blocks_content_font_color_setting' => '#000000',
            'antique_blocks_content_border_color_setting' => '#000000'
        );

        return $defaults;
    }

}

add_action(
        hook_name: 'customize_preview_init',
        callback: 'antique_theme_customizer_colors_js'
);

function antique_theme_customizer_colors_js() {

    wp_register_script(
            handle: 'antique-customizer-color',
            src: get_template_directory_uri() . '/customizer.js',
            deps: array('jquery', 'customize-preview'),
            ver: '',
            in_footer: true
    );

    wp_enqueue_script(handle: 'antique-customizer-color');
}

add_action(
        hook_name: 'wp_head',
        callback: 'antique_theme_customizer_colors_pass_options_to_css'
);

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_theme_customizer_colors_pass_options_to_css'
);

function antique_theme_customizer_colors_pass_options_to_css() {

    $defaults = antique_theme_customizer_default_colors();

    echo '<style id="antique-theme-custom-colors-css">';
    echo ':root{';
    echo '--antique--reading-progress--bg-color-l: ' . get_theme_mod(
            name: 'reading_progress_bg_gradient_l_setting',
            default_value: $defaults['reading_progress_bg_gradient_l_setting']
    ) . ';';
    echo '--antique--reading-progress--bg-color-r: ' . get_theme_mod(
            name: 'reading_progress_bg_gradient_r_setting',
            default_value: $defaults['reading_progress_bg_gradient_r_setting']
    ) . ';';
    echo '--antique--header--bg-color: ' . get_theme_mod(
            name: 'header_bg_color_setting',
            default_value: $defaults['header_bg_color_setting']
    ) . ';';
    echo '--antique--header--font-color: ' . get_theme_mod(
            name: 'header_font_color_setting',
            default_value: $defaults['header_font_color_setting']
    ) . ';';
    echo '--antique--header--font-color--hover: ' . get_theme_mod(
            name: 'header_font_color_hover_setting',
            default_value: $defaults['header_font_color_hover_setting']
    ) . ';';
    echo '--antique--title--bg-color: ' . get_theme_mod(
            name: 'title_bg_color_setting',
            default_value: $defaults['title_bg_color_setting']
    ) . ';';
    echo '--antique--title--bg-color--selection: ' . get_theme_mod(
            name: 'title_bg_color_selection_setting',
            default_value: $defaults['title_bg_color_selection_setting']
    ) . ';';
    echo '--antique--title--font-color: ' . get_theme_mod(
            name: 'title_font_color_setting',
            default_value: $defaults['title_font_color_setting']
    ) . ';';
    echo '--antique--post-nav--bg-color: ' . get_theme_mod(
            name: 'post_nav_bg_color_setting',
            default_value: $defaults['post_nav_bg_color_setting']
    ) . ';';
    echo '--antique--post-nav--font-color: ' . get_theme_mod(
            name: 'post_nav_font_color_setting',
            default_value: $defaults['post_nav_font_color_setting']
    ) . ';';
    echo '--antique--content--bg-color: ' . get_theme_mod(
            name: 'content_bg_color_setting',
            default_value: $defaults['content_bg_color_setting']
    ) . ';';
    echo '--antique--content--font-color: ' . get_theme_mod(
            name: 'content_font_color_setting',
            default_value: $defaults['content_font_color_setting']
    ) . ';';
    echo '--antique--link--color: ' . get_theme_mod(
            name: 'content_link_color_setting',
            default_value: $defaults['content_link_color_setting']
    ) . ';';
    echo '--antique--link--color-visited: ' . get_theme_mod(
            name: 'content_link_visited_color_setting',
            default_value: $defaults['content_link_visited_color_setting']
    ) . ';';
    echo '--antique--footer--bg-color: ' . get_theme_mod(
            name: 'footer_bg_color_setting',
            default_value: $defaults['footer_bg_color_setting']
    ) . ';';
    echo '--antique--footer--font-color: ' . get_theme_mod(
            name: 'title_font_color_setting',
            default_value: $defaults['title_font_color_setting']
    ) . ';';
    echo '--antique--footer--font-color--hover: ' . get_theme_mod(
            name: 'footer_font_color_hover_setting',
            default_value: $defaults['footer_font_color_hover_setting']
    ) . ';';
    echo '--antique-blocks--heading--bg-color: ' . get_theme_mod(
            name: 'antique_blocks_heading_bg_color_setting',
            default_value: $defaults['antique_blocks_heading_bg_color_setting']
    ) . ';';
    echo '--antique-blocks--heading--font-color: ' . get_theme_mod(
            name: 'antique_blocks_heading_font_color_setting',
            default_value: $defaults['antique_blocks_heading_font_color_setting']
    ) . ';';
    echo '--antique-blocks--heading--border-color: ' . get_theme_mod(
            name: 'antique_blocks_heading_border_color_setting',
            default_value: $defaults['antique_blocks_heading_border_color_setting']
    ) . ';';
    echo '--antique-blocks--content--bg-color: ' . get_theme_mod(
            name: 'antique_blocks_content_bg_color_setting',
            default_value: $defaults['antique_blocks_content_bg_color_setting']
    ) . ';';
    echo '--antique-blocks--content--font-color: ' . get_theme_mod(
            name: 'antique_blocks_content_font_color_setting',
            default_value: $defaults['antique_blocks_content_font_color_setting']
    ) . ';';
    echo '--antique-blocks--content--border-color: ' . get_theme_mod(
            name: 'antique_blocks_content_border_color_setting',
            default_value: $defaults['antique_blocks_content_border_color_setting']
    ) . ';';
    echo '}';
    echo '</style>';
}

add_action(
        hook_name: 'customize_register',
        callback: 'antique_theme_customizer_colors_options'
);

function antique_theme_customizer_colors_options($wp_customize) {

    $defaults = antique_theme_customizer_default_colors();

    $transport = 'postMessage';
    $capability = 'edit_theme_options';

    /*
     * Colors
     */

    $wp_customize->add_panel(
            id: 'colors_panel',
            args: array(
                'title' => __('Colors', 'antique'),
            )
    );

    /*
     * Reading progress
     */

    $wp_customize->add_section(
            id: 'reading_progress_colors_section',
            args: array(
                'title' => __('Reading progress bar', 'antique'),
                'panel' => 'colors_panel',
                'capability' => $capability,
            )
    );

    $wp_customize->add_setting(
            id: 'reading_progress_bg_gradient_l_setting',
            args: array(
                'default' => $defaults['reading_progress_bg_gradient_l_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'reading_progress_bg_gradient_l_control',
                    args: array(
                'label' => __('Background gradient: left', 'antique'),
                'section' => 'reading_progress_colors_section',
                'settings' => 'reading_progress_bg_gradient_l_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'reading_progress_bg_gradient_r_setting',
            args: array(
                'default' => $defaults['reading_progress_bg_gradient_r_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'reading_progress_bg_gradient_r_control',
                    args: array(
                'label' => __('Background gradient: right', 'antique'),
                'section' => 'reading_progress_colors_section',
                'settings' => 'reading_progress_bg_gradient_r_setting',
                    )
            )
    );

    /*
     * Header
     */

    $wp_customize->add_section(
            id: 'header_colors_section',
            args: array(
                'title' => __('Header', 'antique'),
                'panel' => 'colors_panel',
                'capability' => $capability,
            )
    );

    $wp_customize->add_setting(
            id: 'header_bg_color_setting',
            args: array(
                'default' => $defaults['header_bg_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'header_bg_color_control',
                    args: array(
                'label' => __('Background color', 'antique'),
                'section' => 'header_colors_section',
                'settings' => 'header_bg_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'header_font_color_setting',
            args: array(
                'default' => $defaults['header_font_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'header_font_color_control',
                    args: array(
                'label' => __('Font color', 'antique'),
                'section' => 'header_colors_section',
                'settings' => 'header_font_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'header_font_color_hover_setting',
            args: array(
                'default' => $defaults['header_font_color_hover_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'header_font_color_hover_control',
                    args: array(
                'label' => __('Font color (hover)', 'antique'),
                'section' => 'header_colors_section',
                'settings' => 'header_font_color_hover_setting',
                    )
            )
    );
    
    /*
     * Title
     */

    $wp_customize->add_section(
            id: 'title_colors_section',
            args: array(
                'title' => __('Title', 'antique'),
                'description' => __(
                        '* Text can be selected or highlighted by clicking and '
                        . 'dragging the mouse across the text.', 'antique'
                ),
                'panel' => 'colors_panel',
                'capability' => $capability,
            )
    );

    $wp_customize->add_setting(
            id: 'title_bg_color_setting',
            args: array(
                'default' => $defaults['title_bg_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'title_bg_color_control',
                    args: array(
                'label' => __('Background color', 'antique'),
                'section' => 'title_colors_section',
                'settings' => 'title_bg_color_setting',
                    )
            )
    );
    
    $wp_customize->add_setting(
            id: 'title_bg_color_selection_setting',
            args: array(
                'default' => $defaults['title_bg_color_selection_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'title_bg_color_selection_control',
                    args: array(
                'label' => __('Background color upon selection*', 'antique'),
                'section' => 'title_colors_section',
                'settings' => 'title_bg_color_selection_setting',
                    )
            )
    );
    
    $wp_customize->add_setting(
            id: 'post_nav_bg_color_setting',
            args: array(
                'default' => $defaults['post_nav_bg_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'post_nav_bg_color_control',
                    args: array(
                'label' => __('Background color', 'antique'),
                'section' => 'post_nav_colors_section',
                'settings' => 'post_nav_bg_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'post_nav_font_color_setting',
            args: array(
                'default' => $defaults['post_nav_font_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'post_nav_font_color_control',
                    args: array(
                'label' => __('Font color', 'antique'),
                'section' => 'post_nav_colors_section',
                'settings' => 'post_nav_font_color_setting',
                    )
            )
    );
    
    /*
     * Post navigation
     */

    $wp_customize->add_section(
            id: 'post_nav_colors_section',
            args: array(
                'title' => __('Post navigation', 'antique'),
                'panel' => 'colors_panel',
                'capability' => $capability,
            )
    );

    $wp_customize->add_setting(
            id: 'post_nav_bg_color_setting',
            args: array(
                'default' => $defaults['post_nav_bg_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'post_nav_bg_color_control',
                    args: array(
                'label' => __('Background color', 'antique'),
                'section' => 'post_nav_colors_section',
                'settings' => 'post_nav_bg_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'post_nav_font_color_setting',
            args: array(
                'default' => $defaults['post_nav_font_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'post_nav_font_color_control',
                    args: array(
                'label' => __('Font color', 'antique'),
                'section' => 'post_nav_colors_section',
                'settings' => 'post_nav_font_color_setting',
                    )
            )
    );
    
    /*
     * Content
     */

    $wp_customize->add_section(
            id: 'content_colors_section',
            args: array(
                'title' => __('Content', 'antique'),
                'panel' => 'colors_panel',
                'capability' => $capability,
            )
    );

    $wp_customize->add_setting(
            id: 'content_bg_color_setting',
            args: array(
                'default' => $defaults['content_bg_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'content_bg_color_control',
                    args: array(
                'label' => __('Background color', 'antique'),
                'section' => 'content_colors_section',
                'settings' => 'content_bg_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'content_font_color_setting',
            args: array(
                'default' => $defaults['content_font_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'content_font_color_control',
                    args: array(
                'label' => __('Font color', 'antique'),
                'section' => 'content_colors_section',
                'settings' => 'content_font_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'content_link_color_setting',
            args: array(
                'default' => $defaults['content_link_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'content_link_color_control',
                    args: array(
                'label' => __('Font color of links', 'antique'),
                'section' => 'content_colors_section',
                'settings' => 'content_link_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'content_link_visited_color_setting',
            args: array(
                'default' => $defaults['content_link_visited_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'content_link_visited_color_control',
                    args: array(
                'label' => __('Font color of visited links', 'antique'),
                'section' => 'content_colors_section',
                'settings' => 'content_link_visited_color_setting',
                    )
            )
    );

    /*
     * Footer
     */

    $wp_customize->add_section(
            id: 'footer_colors_section',
            args: array(
                'title' => __('Footer', 'antique'),
                'panel' => 'colors_panel',
                'capability' => $capability,
            )
    );

    $wp_customize->add_setting(
            id: 'footer_bg_color_setting',
            args: array(
                'default' => $defaults['footer_bg_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'footer_bg_color_control',
                    args: array(
                'label' => __('Background color', 'antique'),
                'section' => 'footer_colors_section',
                'settings' => 'footer_bg_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'footer_font_color_setting',
            args: array(
                'default' => $defaults['footer_font_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'footer_font_color_control',
                    args: array(
                'label' => __('Font color', 'antique'),
                'section' => 'footer_colors_section',
                'settings' => 'footer_font_color_setting',
                    )
            )
    );
    
    $wp_customize->add_setting(
            id: 'footer_font_color_hover_setting',
            args: array(
                'default' => $defaults['footer_font_color_hover_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'footer_font_color_hover_control',
                    args: array(
                'label' => __('Font color (hover)', 'antique'),
                'section' => 'footer_colors_section',
                'settings' => 'footer_font_color_hover_setting',
                    )
            )
    );

    /*
     * Antique blocks
     */

    $wp_customize->add_section(
            id: 'antique_blocks_colors_section',
            args: array(
                'title' => __('Antique blocks', 'antique'),
                'panel' => 'colors_panel',
                'capability' => $capability,
            )
    );

    $wp_customize->add_setting(
            id: 'antique_blocks_heading_bg_color_setting',
            args: array(
                'default' => $defaults['antique_blocks_heading_bg_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'antique_blocks_heading_bg_color_control',
                    args: array(
                'label' => __('Heading background color', 'antique'),
                'section' => 'antique_blocks_colors_section',
                'settings' => 'antique_blocks_heading_bg_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'antique_blocks_heading_font_color_setting',
            args: array(
                'default' => $defaults['antique_blocks_heading_font_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'antique_blocks_heading_font_color_control',
                    args: array(
                'label' => __('Heading font color', 'antique'),
                'section' => 'antique_blocks_colors_section',
                'settings' => 'antique_blocks_heading_font_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'antique_blocks_heading_border_color_setting',
            args: array(
                'default' => $defaults['antique_blocks_heading_border_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'antique_blocks_heading_border_color_control',
                    args: array(
                'label' => __('Heading border color', 'antique'),
                'section' => 'antique_blocks_colors_section',
                'settings' => 'antique_blocks_heading_border_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'antique_blocks_content_bg_color_setting',
            args: array(
                'default' => $defaults['antique_blocks_content_bg_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'antique_blocks_content_bg_color_control',
                    args: array(
                'label' => __('Content background color', 'antique'),
                'section' => 'antique_blocks_colors_section',
                'settings' => 'antique_blocks_content_bg_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'antique_blocks_content_font_color_setting',
            args: array(
                'default' => $defaults['antique_blocks_content_font_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'antique_blocks_content_font_color_control',
                    args: array(
                'label' => __('Content font color', 'antique'),
                'section' => 'antique_blocks_colors_section',
                'settings' => 'antique_blocks_content_font_color_setting',
                    )
            )
    );

    $wp_customize->add_setting(
            id: 'antique_blocks_content_border_color_setting',
            args: array(
                'default' => $defaults['antique_blocks_content_border_color_setting'],
                'transport' => $transport,
                'sanitize_callback' => 'antique_theme_customizer_colors_sanitize_cb',
                'capability' => $capability,
            )
    );

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    manager: $wp_customize,
                    id: 'antique_blocks_content_border_color_control',
                    args: array(
                'label' => __('Content border color', 'antique'),
                'section' => 'antique_blocks_colors_section',
                'settings' => 'antique_blocks_content_border_color_setting',
                    )
            )
    );
}

function antique_theme_customizer_colors_sanitize_cb($color) {
    return sanitize_hex_color($color);
}
