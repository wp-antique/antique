<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of main and all the content after (and ends with the
 * closing tag </html> of the root).
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Antique
 * @since Antique 1.0
 */
?>

</main>

<footer id="colophon" class="site-footer">
    <div class="wrapper">

        <?php get_template_part('template-parts/footer/footer-widget-areas'); ?>
        
        <div class="site-info-wrap">
            <span class="site-info">
                &copy; <?php echo date('Y'); ?> <?php echo get_bloginfo('name'); ?>
                | Powered by
                <a href="https://gitlab.com/wp-antique/antique"
                   target="_blank"><?php echo wp_get_theme(); ?></a>
                Wordpress Theme
            </span>
        </div>

    </div>
</footer>

<?php get_template_part('template-parts/extras/mobile-nav'); ?>

<?php wp_footer(); ?>

</body>
</html>